package com.appselevated.bitlive;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by nathanlarson on 6/1/16.
 */
public class Model_Hib extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DICTIONARY_TABLE_CREATE_1 =
            "CREATE TABLE IF NOT EXISTS " + "Averages" + " (" +
                    "id" + " INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, " +
                    "value" + " DOUBLE);";

    private static final String DICTIONARY_TABLE_CREATE_2 =
            "CREATE TABLE IF NOT EXISTS " + "Personal_Stats" + " (" +
                    "id" + " INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, " +
                    "date" + " DATE, " +
                    "starting_amount" + " DOUBLE, " +
                    "current_value" + " DOUBLE);";

    Model_Hib(Context context) {
        super(context, "db_1", null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DICTIONARY_TABLE_CREATE_1);
        db.execSQL(DICTIONARY_TABLE_CREATE_2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


}

