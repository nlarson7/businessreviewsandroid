package com.appselevated.bitlive;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by nathanlarson on 6/1/16.
 */
public class Controller {

    Model_Soc soc = new Model_Soc();
    Model_Storage modelStorage = new Model_Storage();

    TextView tView;

    // Calls Model_Http
    public Boolean buyBitcoin() {
        return true;
    }

    // Calls Model_Http
    public Boolean sellBitcoin() {
        return true;
    }

    // Calls Model_Soc
    public Void startSocketFeed(TextView view) {
        this.tView = view;
        new FetchDataTask().execute();
//        new Model_Http().execute("https://api.exchange.coinbase.com/products/BTC-USD/book?level=2");
//        soc.execute();
        Log.i("Started Socket Feed", "Started");
        return null;
    }

    // Calls Model_Hib
    public Double retrieveAverage(Context context) {
        Model_Hib hibernate = null;
        if (hibernate == null) {
            hibernate = new Model_Hib(context);
        }
        Log.i("DB Name", hibernate.getDatabaseName());
        return 3.65;
    }

    // Calls Model_Hib
    public Double retrieveMyDailyStartingAmount() {
        return 365.34;
    }

    // Calls Model_Hib
    public Double retrieveMyDailyIncrease() {
        return 2.54;
    }

    // Calls Model_Hib and returns calculation.
    public Double calculateMyDailyEarningPercentage() {
        return 3.5;
    }

    private class FetchDataTask extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... params) {
            JSONObject json = new JSONObject();
            try {
                json =  new Model_Http().getDataFromUrl("https://api.exchange.coinbase.com/products/BTC-USD/book?level=2");
                Log.i("Fetched Contents", json.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject object) {
            // Create my Java Collections using a list and map.
            // A set will contain some other values.
            Log.i("SomeMore", object.toString());

            HashMap<String, Object> tempMap = new HashMap<String, Object>();

            Iterator<String> iter = object.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    Object val = object.get(key);
                    tempMap.put(key, val);

                }catch (JSONException e) {

                }
            }

            Iterator iterator = tempMap.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String)iterator.next();
//                String value = (String)tempMap.get(key);
                Log.i("Result", key);
            }

            Iterator<Map.Entry<String,Object>> iterate = tempMap.entrySet().iterator();
            while (iter.hasNext()) {
                iterate.remove();
            }

            // Tree
            TreeNode<String> root = new TreeNode<String>("root");
            {
                TreeNode<String> node0 = root.addChild("node0");
                TreeNode<String> node1 = root.addChild("node1");
                TreeNode<String> node2 = root.addChild("node2");
                {
                    TreeNode<String> node20 = node2.addChild("node20");
                    TreeNode<String> node21 = node2.addChild("node21");
                    {
                        TreeNode<String> node210 = node20.addChild("node210");
                    }
                }
            }




            root.children.remove(0);



            // Set
            Set<JSONObject> exampleSet = new HashSet<JSONObject>();

            // Add
            exampleSet.add(object);

            // Iterate
            Iterator<JSONObject> it = exampleSet.iterator();
            Integer i = 0;
            while (it.hasNext() & i < 10) {
                Log.i("Set Example", it.toString());
                i++;
            }

            // Remove
            exampleSet.remove(object);

            // List
            List<JSONObject> localExample = new ArrayList<JSONObject>();

            localExample.add(object);

            localExample.remove(0);

            List<JSONObject> storedResults = modelStorage.getStoredResults();

            for (ListIterator<JSONObject> iterateJson = storedResults.listIterator(); iterateJson.hasNext(); ) {
                JSONObject element = iterateJson.next();
            }

            storedResults.add(object);

            modelStorage.setStoredResults(storedResults);

            // Get all results again.
            List<JSONObject> myDataPoints = modelStorage.getStoredResults();

            // Get the last one
            JSONObject obj = myDataPoints.get(myDataPoints.size() - 1);
            // Get the first bid.
            try {
                JSONArray tempList = (JSONArray) obj.get("bids");
                JSONArray numberSet = (JSONArray) tempList.get(0);
                String number = (String) numberSet.get(0);
                tView.setText("$" + number);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // Set the textview.

        }
    }

    // Tree
    public class TreeNode<T> implements Iterable<TreeNode<T>> {

        T data;
        TreeNode<T> parent;
        List<TreeNode<T>> children;

        public TreeNode(T data) {
            this.data = data;
            this.children = new LinkedList<TreeNode<T>>();
        }

        public TreeNode<T> addChild(T child) {
            TreeNode<T> childNode = new TreeNode<T>(child);
            childNode.parent = this;
            this.children.add(childNode);
            return childNode;
        }

        @Override
        public Iterator<TreeNode<T>> iterator() {
            return null;
        }

        // other features ...

    }

}
