package com.appselevated.bitlive;

import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nathanlarson on 7/15/16.
 */

// This is my Java Bean Class - a helper class.

public class Model_Storage {

    List<JSONObject> storedResults = new ArrayList<JSONObject>();

    public List<JSONObject> getStoredResults()
    {
        return this.storedResults;
    }

    public void setStoredResults(List<JSONObject> list)
    {
        this.storedResults = list;
        Log.w("StoredJSONCount", "Size:" + this.storedResults.size());
    }

}
